package pl.wiyarmir.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements ICalculator {

	protected TextView textBox;
	protected Calculator calculator;
	private ArrayAdapter<String> adapter;
	private ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_landscape);

		textBox = (TextView) findViewById(R.id.textBox);
		initKeys();

		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1);

		lv = (ListView) findViewById(R.id.list);

		lv.setAdapter(adapter);
		lv.setStackFromBottom(true);
		lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

		calculator = new Calculator(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_clear:
			adapter.clear();
			clearCalculatorScreen();
			Toast.makeText(this, "Operation history cleared.",
					Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private static final int[] numberButtonsIds = { R.id.button0, R.id.button1,
			R.id.button2, R.id.button3, R.id.button4, R.id.button5,
			R.id.button6, R.id.button7, R.id.button8, R.id.button9 };

	private void initKeys() {
		for (int i = 0; i < numberButtonsIds.length; i++) {
			((Button) findViewById(numberButtonsIds[i]))
					.setOnClickListener(new NumberButtonListener(i));
		}

		((Button) findViewById(R.id.buttonClr))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						calculator.clear();
					}
				});

		((Button) findViewById(R.id.buttonDot))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						textBox.append(".");
					}
				});
		((Button) findViewById(R.id.buttonDiv))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						calculator.division();
					}
				});
		((Button) findViewById(R.id.buttonMul))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						calculator.multiply();
					}
				});
		((Button) findViewById(R.id.buttonMin))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						calculator.minus();
					}
				});
		((Button) findViewById(R.id.buttonPlus))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						calculator.plus();
					}
				});
		((Button) findViewById(R.id.buttonEq))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						calculator.equals();
					}
				});
	}

	class NumberButtonListener implements View.OnClickListener {

		private int num;

		public NumberButtonListener(int i) {
			num = i;
		}

		@Override
		public void onClick(View v) {
			calculator.numberPressed();
			textBox.append(String.valueOf(num));
		}

	}

	@Override
	public void clearCalculatorScreen() {
		textBox.setText("");
	}

	@Override
	public void showNumber(float n) {
		textBox.setText(String.valueOf(n));
	}

	@Override
	public float valueOfScreen() {
		String t = textBox.getText().toString();
		if (t.equals("")) {
			return 0;
		} else {
			return Float.parseFloat(t);
		}
	}

	@Override
	public void pushOperation(String op) {
		adapter.add(op);
	}

}
