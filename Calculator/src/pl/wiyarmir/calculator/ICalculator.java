package pl.wiyarmir.calculator;

public interface ICalculator {
	public abstract void clearCalculatorScreen();

	public abstract void showNumber(float n);

	public abstract float valueOfScreen();

	public abstract void pushOperation(String op);
}
