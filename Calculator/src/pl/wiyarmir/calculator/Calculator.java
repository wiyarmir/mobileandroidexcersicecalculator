package pl.wiyarmir.calculator;

public class Calculator {
	float lastnumber = 0;
	char lastOperation = ' ';
	ICalculator ic;
	private boolean firstNumberInOperation = true;

	public Calculator(ICalculator c) {
		ic = c;
	}

	public void result() {
		if (ic.valueOfScreen() != 0) {
			switch (lastOperation) {
			case '+':
				ic.showNumber(lastnumber + ic.valueOfScreen());
				break;
			case '-':
				ic.showNumber(lastnumber - ic.valueOfScreen());
				break;
			case '/':
				ic.showNumber(lastnumber / ic.valueOfScreen());
				break;
			case 'x':
				ic.showNumber(lastnumber * ic.valueOfScreen());
				break;

			}
		}
		firstNumberInOperation = true;
	}

	public void plus() {
		result();
		lastOperation = '+';
		ic.pushOperation("" + ic.valueOfScreen());
		lastnumber = ic.valueOfScreen();

	}

	public void minus() {
		result();
		lastOperation = '-';
		ic.pushOperation("" + ic.valueOfScreen());
		lastnumber = ic.valueOfScreen();

	}

	public void multiply() {
		result();
		lastOperation = 'x';
		ic.pushOperation("" + ic.valueOfScreen());
		lastnumber = ic.valueOfScreen();

	}

	public void division() {
		result();
		lastOperation = '/';
		ic.pushOperation("" + ic.valueOfScreen());
		lastnumber = ic.valueOfScreen();

	}

	public void clear() {
		result();
		lastnumber = 0;
		lastOperation = ' ';
		ic.clearCalculatorScreen();
	}

	public void numberPressed() {
		if (firstNumberInOperation) {
			ic.pushOperation("" + lastOperation);
			ic.clearCalculatorScreen();
			firstNumberInOperation = false;
		}
	}

	public void equals() {
		ic.pushOperation("" + ic.valueOfScreen());
		result();
	}
}
